# belajar-kampus-merdeka



# Prerequisite
0. Git (https://git-scm.com/downloads) (You don't say)
1. UNIX (Mac/Linux) or Windows with WSL supported 
2. Docker (https://docs.docker.com/engine/install/)
3. Your favorite text editor / IDE (Visual Studio Code / Sublime / Intellij)
4. Min 8GB RAM
5. Ready to learn!

## Getting started
1. Clone this repository
2. Go to project root directory
3. Run sail 
```
./vendor/bin/sail up
```
4. Wait for sail is up. After that, run this in CLI docker container (sail/app)
```
npm install
npm run dev
php artisan migrate
```

4. Go to public/hot, change the http://0.0.0.0:5173 to http://localhost:5173
5. Check your http://localhost
